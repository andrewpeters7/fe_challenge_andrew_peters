import React from 'react';
import { Modal } from 'react-bootstrap';
import { connect } from 'react-redux';
import { hideModal, editShift } from '../../actions';
import Select from 'react-select';
import { TimePicker } from 'antd';
import moment from 'moment-timezone';

const format = 'h:mm A'; //moment.js formatting for time-pickers

class ShiftModal extends React.Component {
    state = {
        error: null,
    }

    submitForm(){
        if(this.form.endTime.isAfter(this.form.startTime) === false){
            // @TODO, account for break_duration in here?
            return this.setState({error: 'The selected shift start time has to preceed the end time.'})
        }
        
        // If times are valid, fire the shift update action creator.
        this.props.editShift(this.props.shift.id, this.form.startTime, this.form.endTime);
        this.closeModal();
    }

    closeModal(){
        // The component doesn't unmount, so cleanup is done in the closeModal sequence.
        this.props.hideModal(); // Call this first as it prevents full re-renders.
        this.setState({ error: null });
    }

    TimePicker(binding){
        return (
            <TimePicker
                defaultValue={this.form[binding]}
                onChange={(moment) => this.form[binding] = moment}
                minuteStep={5}
                use12Hours
                format={format}
                allowClear={false}
            />
        )
    }

    BreakTimePicker(breakTime){
        const time = `${Math.floor(breakTime / 3600)}:${Math.floor((breakTime % 3600)/60)}`
        const format = 'H:mm';
        return (
            <TimePicker
                defaultValue={moment(time, format)}
                minuteStep={5}
                format={format}
                allowClear={false}
                disabled
            />
        )
    }


    render() {
        const { shift, timezone } = this.props;

        if (shift === null) {
            // Prevent rendering if there's no shift to display.
            return null;
        }

        const employeeValue = {
            value: shift.employee_id,
            label: `${shift.employee.first_name} ${shift.employee.last_name}`
        }

        const roleValue = {
            value: shift.role_id,
            label: shift.role.name
        }

        this.form = {
            startTime: moment(shift.start_time).tz(timezone),
            endTime: moment(shift.end_time).tz(timezone),
        }

        return (
            <div className="ShiftModal">
                <Modal show={shift !== null} onHide={() => this.closeModal()}>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit Shift</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="alert alert-danger" hidden={this.state.error === null}>
                            <span>{this.state.error}</span>
                        </div>

                        <div className="form-group">
                            <label>Employee</label>
                            <Select
                                value={employeeValue}
                                className="d-block"
                                isDisabled
                            />
                        </div>

                        <div className="form-group">
                            <label>Role</label>
                            <Select
                                value={roleValue}
                                className="d-block"
                                isDisabled
                            />
                        </div>

                        <div className="row">
                            <div className="col-4">
                                <div className="form-group">
                                    <label>Start Time</label><br />
                                    {this.TimePicker('startTime')}
                                </div>
                            </div>
                            <div className="col-4">
                                <div className="form-group">
                                    <label>End Time</label><br />
                                    {this.TimePicker('endTime')}
                                </div>
                            </div>
                            <div className="col-4">
                                <div className="form-group">
                                    <label>Break Time</label><br />
                                    {this.BreakTimePicker(shift.break_duration)}
                                </div>
                            </div>
                        </div>

                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-secondary" onClick={() => this.closeModal()}>
                            Close
                        </button>
                        <button className="btn btn-primary" onClick={() => this.submitForm()}>
                            Save Changes
                        </button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}


const mapStateToProps = ({ modal_shift, timezone }) => ({
    shift: modal_shift,
    timezone,
})

export default connect(mapStateToProps, { hideModal, editShift })(ShiftModal);