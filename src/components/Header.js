import React from 'react';
import StoreSelect from './StoreSelect';
import { NavLink } from "react-router-dom";

class Header extends React.Component {
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <span className="navbar-brand">Rostering Demo App</span>

                <div className="collapse navbar-collapse">
                    <div className="ml-auto">
                        <StoreSelect />
                    </div>
                    
                    <ul className="navbar-nav ml-2">
                        <li className="nav-item">
                            <NavLink to="/" className="nav-link" exact activeClassName="active">Table View</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/calendar" className="nav-link" exact activeClassName="active">Calendar View</NavLink>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }
}



export default Header;