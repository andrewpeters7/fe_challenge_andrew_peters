import React from 'react';
import { connect } from 'react-redux'
import moment from 'moment-timezone';
import {showModal} from '../../actions';

import './CalendarView.scss';

const targetDate = (day) => [2018, 5, 17 + day]; // Moment.js can be initialised with a [YYYY, MM, DD] array. DD can be > 31.
const calendarLength = 9; // days

// The below function is placed outside the component to avoid performance drawbacks from instance reinitialising.
// I'm unsure if this is a best-practice within React.
const TableHead = () => {
    const tableHeaders = []

    for (let i = 0; i < calendarLength; i++) {
        const day = moment(targetDate(i)); // No need for timezone here.

        const tableCell = (
            <th key={'header-' + i} className="text-center shift-cell-th">
                <span>{day.format('dddd')}</span><br />
                <span className="date-header">{day.format('LL')}</span>
            </th>
        )

        tableHeaders.push(tableCell)
    }

    return tableHeaders;
}

const getShiftDayDifference = (shift, next, timezone) => {
    // Returns -1 if no next shift
    // Returns 0 if next shift is on the same day
    // Else, returns num of days (integer)

    if (next === undefined) {
        return -1;
    }

    const shiftDay = moment.tz(shift.start_time, timezone).startOf('day');
    const nextDay = moment.tz(next.start_time, timezone).startOf('day');

    return Math.abs(nextDay.diff(shiftDay, 'days'));
}

const getShiftHourDifference = (shift, next) => {
    // Returns -1 if no next shift
    // Returns 0 if next shift is on the same day
    // Else, returns num of days (integer)

    if (next === undefined) {
        return -1;
    }

    const shiftEnd = moment(shift.end_time) // Timezone not necessary
    const nextStart = moment(next.start_time) // Timezone not necessary

    return Math.abs(shiftEnd.diff(nextStart, 'hours'));
}

const formatSeconds = (time) => {
    // Used by ShiftCell and TableBody
    const mins = Math.floor((time % 3600) / 60);
    const hours = Math.floor(time / 3600);

    let text = '';

    if (hours > 0) {
        text += `${hours} hr${hours > 1 ? 's' : ''} `
    }

    if (mins > 0) {
        text += `${mins} min${mins > 1 ? 's' : ''}`
    }

    return text;
}

const getTotalSeconds = (shifts) => {
    // Tallys up the hours of all shifts.
    // Excludes break durations.

    // Returns seconds

    let counter = 0;

    for (const { start_time, end_time, break_duration } of shifts) {
        const total = Math.abs(moment(start_time).diff(moment(end_time), 'seconds')) // Timezone not necessary

        counter += (total - break_duration) // Break duration is seconds and not a date string.
    }

    return counter;
}

class CalenderView extends React.Component {

    ShiftCell(shift, daysTilNext, hoursTilNext){
        const {timezone, showModal} = this.props,
            { text_colour, background_colour } = shift.role;
        
        const cellStyling = {
            backgroundColor: background_colour,
            color: text_colour
        }

        const breakTime = formatSeconds(shift.break_duration),
            start_time = moment.tz(shift.start_time, timezone).format('h:mm A'),
            end_time = moment.tz(shift.end_time, timezone).format('h:mm A'),
            showHoursBetween = daysTilNext <= 1 && hoursTilNext !== -1,
            restWrapClasses = `text-right rest-wrap px-1 ${hoursTilNext < 12 ? 'danger' : ''}`;
        // If daysTilNext > 1, then don't worry about displaying hour difference.

        return (
            <div className="shift-cell" style={cellStyling} onClick={() => showModal(shift)}>
                <div className="d-flex justify-content-between align-items-center">
                    <span className="role-name">{shift.role.name}</span>
                    <a href="javascript:;" className="text-uppercase shift-edit">Edit</a>
                </div>
                <div>
                    <div>
                        <span>{start_time} - {end_time}</span>
                    </div>
                    <div>{breakTime} break</div>
                    <div hidden={showHoursBetween === false} className={restWrapClasses}>
                        <hr />
                        <span>{hoursTilNext} hr{hoursTilNext > 1 ? 's' : ''} rest</span> <i className="fas fa-arrow-right"></i>
                    </div>
                </div>
            </div>
        )
    }

    EmployeeCells(shifts, employeeID) {
        const { timezone } = this.props;
        // employeeID only used for cell keys.

        // Note that these shifts are already sorted start_time ascending.
        const cells = [];
        let lCounter = 0; // Used to track start position in the shifts array.

        // For this specific case, we could create a 9-element array with all
        // of the pre-created dates, which would save some computing, but would be a bit messier.
        for (let i = 0; i < calendarLength; i++) {
            const cellTime = moment.tz(targetDate(i), timezone)

            const cell = []

            for (let j = lCounter; j < shifts.length; j++) {
                // We could use the array prototype method 'some' here, but it's sometimes less obvious.
                const shiftTime = moment.tz(shifts[j].start_time, timezone).startOf('day');

                if (cellTime.isSame(shiftTime)) {
                    // I.e., 10/07/2018 === 10/07/2018
                    const currentShift = shifts[j];
                    const nextShift = shifts[j + 1];

                    // Days until next shift
                    const daysTilNext = getShiftDayDifference(currentShift, nextShift, timezone);
                    const hoursTilNext = getShiftHourDifference(currentShift, nextShift); // No timezone needed

                    cell.push(this.ShiftCell(currentShift, daysTilNext, hoursTilNext))

                    lCounter = j + 1;

                    if (daysTilNext !== 0) {
                        // Unless there's another shift on this same day, we'll break.
                        break;
                    }
                }

                if (cellTime.isBefore(shiftTime)) {
                    // Then break, as shiftTimes will only increase due to ascending order.
                    break;
                }
            }

            cells.push(<td key={`${employeeID}-${i}`} className="shift-cell-td">{cell}</td>);
        }

        return cells;
    }

    TableBody() {
        const { employees, shifts, timezone } = this.props;

        return employees.map(employee => {
            const employeeName = `${employee.first_name} ${employee.last_name}`;
            const employeeShifts = shifts
                .filter(({ employee_id }) => employee_id === employee.id)
                .sort((a, b) => new Date(a.start_time) - new Date(b.start_time)); // Sort ascending.
            // employeeShifts is very inefficient, but is pretty readable.

            const totalWeekHours = formatSeconds(getTotalSeconds(employeeShifts));

            return (
                <tr key={employeeName}>
                    <th>
                        <div>{employeeName}</div>
                        <div className="date-header">
                            {totalWeekHours}<br />
                            (exc. breaks)
                        </div>
                    </th>
                    {this.EmployeeCells(employeeShifts, employee.id)}
                </tr>
            )
        })
    }

    render() {
        return (
            <div className="CalenderView">
                <div className="container-fluid">
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Employees</th>
                                {TableHead()}
                            </tr>
                        </thead>
                        <tbody>
                            {this.TableBody()}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

const mapStateToProps = ({ timezone, employees, shifts }) => {
    return {
        timezone,
        employees,
        shifts
        //We don't need Roles here as we've already joined roles to shifts.
    };
}

export default connect(mapStateToProps, { showModal })(CalenderView);