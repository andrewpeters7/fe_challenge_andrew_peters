import React from 'react';
import { connect } from 'react-redux'
import moment from 'moment-timezone'
import { updateSort, showModal } from '../../actions'
import TableHeader from './TableHeader'

import './TableView.scss';

let sortKey;

class TableView extends React.Component {

    setSort(newKey) {
        // If the newKey is the same as the currentKey, make the sort a reverse sort.
        sortKey = sortKey === newKey ? `-${newKey}` : newKey;

        this.props.updateSort(sortKey)
    }

    shiftRows() {
        return this.props.shifts.map(shift => {

            return (
                <tr key={shift.id}>
                    <td>{shift.employee.first_name} {shift.employee.last_name}</td>
                    <td>{shift.role.name}</td>
                    <td>{moment(shift.start_time).tz(this.props.timezone).format('LL')}</td>
                    <td>{moment(shift.start_time).tz(this.props.timezone).format('LT')}</td>
                    <td>{moment(shift.end_time).tz(this.props.timezone).format('LT')}</td>
                    <td>
                        <button className="btn btn-sm btn-primary edit-button" onClick={() => this.props.showModal(shift)}>
                            <i className="fas fa-pencil-alt mr-2"></i>
                            <span>Edit</span>
                        </button>
                    </td>
                </tr>
            )
        })
    }

    render() {
        return (
            <div className="TableView">
                <div className="container">
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col" onClick={() => this.setSort('employee.name')}>
                                    <TableHeader label="Name" />
                                </th>
                                <th scope="col" onClick={() => this.setSort('role.name')} >
                                    <TableHeader label="Role" />
                                </th>
                                <th scope="col" onClick={() => this.setSort('start_time')}>
                                    <TableHeader label="Shift Date" />
                                </th>
                                <th scope="col">Start Time</th>
                                <th scope="col">End Time</th>
                                <th scope="col">
                                    <span hidden>Actions</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.shiftRows()}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

const mapStateToProps = ({ timezone, roles, employees, shifts }) => {
    return {
        timezone,
        roles,
        employees,
        shifts
    };
}

export default connect(mapStateToProps, { updateSort, showModal })(TableView);