import React from 'react';

class TableHeader extends React.Component {
    render (){
        const {label} = this.props;
        return (
            <span>{label}<i className="fas fa-sort ml-1"></i></span>
        )
    }
}

export default TableHeader;