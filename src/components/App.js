import React from 'react';
import Header from './Header';
import TableView from './TableView';
import ShiftModal from './ShiftModal';
import CalendarView from './CalendarView';
import { BrowserRouter as Router, Route } from "react-router-dom";

import './App.scss'

class App extends React.Component {
    render() {
        return (
            <Router>
            <div className="App">
                <Header />
                    <div className="py-5">
                        <Route path="/" exact component={TableView} />
                        <Route path="/calendar" exact component={CalendarView} />
                    </div>
                <ShiftModal />
            </div>
            </Router>
        )
    }
}

export default App;