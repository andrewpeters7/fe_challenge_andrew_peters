import React from 'react';
import Select from 'react-select';
import { connect } from 'react-redux'
import { selectTimezone} from '../../actions'

import './StoreSelect.scss';

class StoreSelect extends React.Component {

    handleChange = (selectedOption) => {
        //Call action creator.
        this.props.selectTimezone(selectedOption.value);
    }

    render() {
        const { stores, timezone } = this.props;
        const selectedOption = stores.find(({ value }) => value === timezone);

        // Note, in cases like these, I'm unsure if the react-way dictates setting changes to
        // both the state and redux store. IMO, it feels cleaner just to update redux, but
        // in this scenario it means we have to re-run the 'find' method on the stores.

        return (
            <div className="StoreSelect">
                <Select
                    value={selectedOption}
                    onChange={this.handleChange}
                    options={stores}
                    className="employee-select-wrap"
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        timezone: state.timezone,
        stores: state.stores
    };
}

export default connect(mapStateToProps, {selectTimezone})(StoreSelect);