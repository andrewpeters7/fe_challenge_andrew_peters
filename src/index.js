import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {createStore} from 'redux';

import App from './components/App';
import reducers from './reducers'

import 'bootstrap/dist/css/bootstrap.min.css';
import 'antd/dist/antd.css';

// Take the react component and show it on the screen
ReactDOM.render(
    <Provider store={createStore(reducers)}>
        <App />
    </Provider>, document.querySelector('#root')
)