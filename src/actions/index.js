export const selectTimezone = (timezone) => {
    return {
        type: 'TIMEZONE_SELECTED',
        payload: timezone
    }
}

export const editShift = (id, start_time, end_time) => {
    return {
        type: 'SHIFT_EDITED',
        payload: {
            id,
            start_time,
            end_time
        }
        
    }
}

export const updateSort = (key) => {
    return {
        type: 'SORT_SELECTED',
        payload: key
    }
}

export const showModal = (shift) => {
    return {
        type: 'OPEN_MODAL',
        payload: shift
    }
}

export const hideModal = () => {
    return {
        type: 'HIDE_MODAL'
    }
}