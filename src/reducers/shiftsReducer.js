import ShiftsJSON from '../files/shifts.json';
import EmployeesJSON from '../files/employees.json';
import RolesJSON from '../files/roles.json';
import tableSort from '../utils/tableSort';

const joinedShifts = ShiftsJSON.map(shift => {
    // Attach all necessary information initially. This probably isn't a best-practice
    // for Redux, but I can't think of any other way to join the collections without
    // firing a lot of attaching functions within components.
    shift.employee = EmployeesJSON.find(({ id }) => id === shift.employee_id);
    shift.role = RolesJSON.find(({ id }) => id === shift.role_id);

    return shift;
})


const shiftsReducer = (shifts = joinedShifts, action) => {
    switch (action.type) {
        case 'SHIFT_EDITED':
            // I know that I should probably be doing some checks
            // here to see if the action contains the properties that
            // I'm expecting it to have.

            const {start_time, end_time, id} = action.payload;

            return [...shifts].map(shift => {
                if (shift.id === id) {
                    return {
                        ...shift,
                        start_time,
                        end_time
                    }
                }

                return shift;
            })

        case 'SORT_SELECTED':
            
            const key = action.payload

            if (key.includes('-')) {
                // Then it must be a reverse sort.
                // Therefore, it's also going to be the same
                // as the previous sort, just reversed. (As the reverse sort can only be reached via a double-click)

                return [...shifts].reverse()
            }


            if (key === 'employee.name') {
                // Sort by first name, and then last name - in order to have a listed sorted primarily by last name,
                // and secondarily sorted by first name
                return [...shifts].sort(tableSort('employee.first_name')).sort(tableSort('employee.last_name'))
            }

            // Sort regularly. 
            // Times are safe to sort as they're strings and not Date Objects/ number of milliseconds
            return [...shifts].sort(tableSort(key))
        
        default:
            return shifts
    }


}

export default shiftsReducer;