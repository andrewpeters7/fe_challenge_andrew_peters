import { combineReducers } from 'redux';
import shiftsReducer from './shiftsReducer'
import StoresJSON from '../files/config.json'
import EmployeesJSON from '../files/employees.json'
import RolesJSON from '../files/roles.json'

const stores = StoresJSON.map(({ timezone, location }) => ({
    label: location,
    value: timezone
}))

const storesReducer = () => {
    // I'm unsure if static data has any place in Redux at all.
    // But it seems easier to put all of it in here.
    // It's probably not the most compute-efficient approach though.
    return stores;
}

const timezoneReducer = (timezone = 'Australia/Perth', action) => {
    if (action.type === 'TIMEZONE_SELECTED') {
        return action.payload;
    }

    return timezone;
}

const modalShiftReducer = (state = null, action) => {
    switch(action.type){
        case 'OPEN_MODAL':
            return action.payload
        case 'HIDE_MODAL':
            return null;
        default:
            return state;
    }
}

export default combineReducers({
    timezone: timezoneReducer,
    stores: storesReducer,
    shifts: shiftsReducer,
    modal_shift: modalShiftReducer,
    roles: () => RolesJSON,
    employees: () => EmployeesJSON
})