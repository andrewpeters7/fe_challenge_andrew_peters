const sortCheck = (a, b) => {
    const acceptedDataTypes = ['boolean', 'string', 'number'];
    const aValid = acceptedDataTypes.includes(typeof a);
    const bValid = acceptedDataTypes.includes(typeof b);

    if (aValid === false && bValid === false) {
        return 0;
    }

    if (aValid === false || a < b) {
        return -1;
    }

    if (bValid === false || a > b) {
        return 1;
    }

    return 0;
}

const tableSort = (sortKey) => {
    // Pass this function a string like 'key.key' in order to do deeper sorts.

    return (a, b) => {
        const keys = sortKey.split('.');
        let nameA = a;
        let nameB = b;

        keys.forEach(key => {
            if (typeof nameA === 'object') {
                if (Array.isArray(nameA) === true) {
                    nameA = nameA[+key];
                } else {
                    nameA = nameA[key];
                }
            }

            if (typeof nameB === 'object') {
                if (Array.isArray(nameB) === true) {
                    nameB = nameB[+key];
                } else {
                    nameB = nameB[key];
                }
            }
        });
        
        //Make the sort check case-insensitive.
        if (typeof nameA === 'string') {
            nameA = nameA.toUpperCase();
        }

        if (typeof nameB === 'string') {
            nameB = nameB.toUpperCase();
        }

        return sortCheck(nameA, nameB);
    };
}

export default tableSort;